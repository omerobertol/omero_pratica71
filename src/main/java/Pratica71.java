import java.util.ArrayList;
import java.util.Scanner;
import utfpr.ct.dainf.if62c.pratica.Jogador;

public class Pratica71 {

  public static void main(String[] args) {
    boolean achou;
    int i, n, numero;
    String nome;
    
    ArrayList<Jogador> time = new ArrayList<>();
    
    Scanner ler = new Scanner(System.in);
    // solicite que o usuário digite o número de jogadores a serem lidos
    while (ler.hasNextInt() == false) {
      ler.nextLine();  
    }
    n = ler.nextInt();
    
    for (i=0; i<n; i++) {
      if (ler.hasNextInt()) {
         numero = ler.nextInt();

         ler.nextLine(); // esvazia o buffer do teclado

         nome = ler.nextLine();
         
         time.add(new Jogador(numero, nome));
      }
      else ler.nextLine();
    }
    
    time.sort(null);
    
    while (true) {
      // exibe a lista
      for (i=0; i<time.size(); i++) {
        System.out.println(time.get(i).toString());  
      }
      
      if (ler.hasNextInt()) {
         numero = ler.nextInt();
         if (numero == 0)
            break;
         
         ler.nextLine(); // esvazia o buffer do teclado
         
         nome = ler.nextLine();
         
         i = 0;
         achou = false;
         while ((i < time.size()) && (achou == false)) {
           if (time.get(i).getNumero() == numero)
              achou = true;
           else if (time.get(i).getNumero() > numero)
                   break;
                else i++;
         }
         
         if (achou == true)
            time.get(i).setNome(nome);
         else time.add(i, new Jogador(numero, nome));
      }  
    }   
  } 
}